<?php


namespace Wgroupe\App\Models;


use Wgroupe\Modules\Orm\Model;

/**
 * Class User
 * @package Wgroupe\App\Models
 */
class User extends Model
{
    // User ROLES
    const ROLE_USER  = 'user';
    const ROLE_ADMIN = 'admin';

    /**
     * User table from DB
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * Autoincrement PK
     * @var
     */
    public $id;

    /**
     * User name
     *
     * @var
     */
    public $name;

    /**
     * User role | DEFAULT user
     *
     * @var string
     */
    public $role = self::ROLE_USER;

    /**
     * User email uniq
     *
     * @var
     */
    public $email;

    /**
     * User password HASH
     *
     * @var
     */
    public $password;

    /**
     * Find user by email
     *
     * @param string $email
     * @return array|null
     */
    public function getByEmail(string $email): ?array
    {
        return $this
            ->where('email', $email)
            ->one();
    }

    public function getAccessToken()
    {
        return $this->from('user_tokens')
            ->where('user_id', $this->id)
            ->one();
    }

}