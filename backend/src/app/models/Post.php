<?php


namespace Wgroupe\App\Models;


use Wgroupe\Modules\Orm\Model;

/**
 * Class Post
 * @package Wgroupe\App\Models
 */
class Post extends Model
{
    const POST_STATUS_DRAFT      = 'draft';
    const POST_STATUS_MODERATION = 'moderation';
    const POST_STATUS_APPROVED   = 'approved';
    const POST_STATUS_PUBLISHED  = 'published';

    /**
     * User table from DB
     *
     * @var string
     */
    protected $table = 'posts';

    /**
     * Get all statuses
     * @return array|string[]
     */
    public static function getStatuses(): array
    {
        return [
            self::POST_STATUS_DRAFT      => self::POST_STATUS_DRAFT,
            self::POST_STATUS_MODERATION => self::POST_STATUS_MODERATION,
            self::POST_STATUS_APPROVED   => self::POST_STATUS_APPROVED,
            self::POST_STATUS_PUBLISHED  => self::POST_STATUS_PUBLISHED
        ];
    }

    public function getAll()
    {
        return $this->many();
    }

    public function getByTitle(string $title)
    {
        return $this->where('title', $title)
            ->one();
    }
}