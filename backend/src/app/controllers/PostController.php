<?php


namespace Wgroupe\App\Controllers;


use Wgroupe\App\Repositories\Post\PostRepository;
use Wgroupe\Modules\Router\Components\Request;

/**
 * Class PostController
 * @package Wgroupe\App\Controllers
 */
class PostController
{
    /**
     * @var PostRepository
     */
    private $postRepository;

    /**
     * PostController constructor.
     */
    public function __construct()
    {
        $this->postRepository = new PostRepository();
    }

    public function index(Request $request)
    {
        return response()->json(
            $this->postRepository->paginate()
        );
    }

    public function show(string $postName)
    {
        return response()->json(
            $this->postRepository->getPost($postName)
        );
    }

    public function update($postId, Request $request)
    {

    }

    public function create(Request $request)
    {

    }

    public function delete()
    {

    }
}