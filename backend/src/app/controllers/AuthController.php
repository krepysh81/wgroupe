<?php


namespace Wgroupe\App\Controllers;


use Wgroupe\App\Repositories\Auth\AuthRepository;
use Wgroupe\Modules\Router\Components\Request;

/**
 * Class AuthController
 * This class handles methods for authorizing a user in the system
 *
 * @package Wgroupe\App\Controllers
 */
class AuthController
{
    /**
     * Processing logic in the repository
     *
     * @var AuthRepository
     */
    private $authRepository;

    /**
     * Init auth repository
     * AuthController constructor.
     */
    public function __construct()
    {
        $this->authRepository = new AuthRepository();
    }

    /**
     * Login action method
     *
     * @url: auth/login
     * @param Request $request
     * @return string
     */
    public function login(Request $request)
    {
        if (!$request->email) {
           return response()->validationError([
               'email' => 'Email field is require'
           ]);
        }

        if (!filter_var($request->email, FILTER_VALIDATE_EMAIL)) {
            return response()->validationError([
                'email' => 'Email field has bad format'
            ]);
        }

        if (!$request->password) {
            return response()->validationError([
                'password' => 'Password field is require'
            ]);
        }

        $user = $this->authRepository->login(
            $request->only([
                'email', 'password'
            ])
        );

        if (!$user) {
            return response()->json([
                'message' => 'Login  fail!'
            ], 400);
        }

        return response()->json([
            'user' => $user
        ]);

    }

    /**
     * Registration action method
     *
     * @url: auth/registration
     * @param Request $request
     */
    public function registration(Request $request)
    {
        if (!$request->email) {
            return response()->validationError([
                'email' => 'Email field is require'
            ]);
        }

        if (!filter_var($request->email, FILTER_VALIDATE_EMAIL)) {
            return response()->validationError([
                'email' => 'Email field has bad format'
            ]);
        }

        if (!$request->name) {
            return response()->validationError([
                'name' => 'Name field is require'
            ]);
        }

        if (!$request->password) {
            return response()->validationError([
                'password' => 'Password field is require'
            ]);
        }

        if (!$request->password_repeat) {
            return response()->validationError([
                'password_repeat' => 'Password repeat field is require'
            ]);
        }

        if ($request->password !== $request->password_repeat) {
            return response()->validationError([
                'password_repeat' => 'Password mismatch'
            ]);
        }

        $user = $this->authRepository->registration(
            $request->only([
                'email',
                'name',
                'password',
            ])
        );

        if (!$user) {
            return response()->json([
                'message' => 'Registration fail!'
            ], 400);
        }

        return response()->json($user);

    }
}