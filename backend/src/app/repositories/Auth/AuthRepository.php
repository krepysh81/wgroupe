<?php


namespace Wgroupe\App\Repositories\Auth;


use Wgroupe\App\Models\User;

/**
 * Class AuthRepository
 * @package Wgroupe\App\Repositories\Auth
 */
class AuthRepository
{
    /**
     * User model
     *
     * @var User
     */
    private $user;

    /**
     * AuthRepository constructor.
     */
    public function __construct()
    {
        $this->user = new User();
    }

    /**
     * Process authorization user in system
     *
     * @param array $data
     * @return array|null
     */
    public function login(array $data): ?array
    {
        $user = $this->user->getByEmail($data['email']);
        if (!$user) {
            return null;
        }

        if (!$this->_checkPassword($data['password'], $user['password'])) {
            return null;
        }

//        $this->user->id = $user['id'];
//        var_dump($this->user->getAccessToken());die();;
        return $user;

    }


    public function registration(array $data)
    {
        $newUser = array(
            'name'     => $data['name'],
            'email'    => $data['email'],
            'role'     => User::ROLE_USER,
            'password' => $this->_prepareUserPassword($data['password'])
        );

       $res = $this->user->insert($newUser)->execute();

       return [];
    }

    /**
     * Сheck passwords for matches
     *
     * @param $password
     * @param $currentPassword
     * @return bool
     */
    private function _checkPassword($password, $currentPassword): bool
    {
        return password_verify($password, $currentPassword);
    }

    /**
     * Prepare a password and convert it to a hash
     *
     * @param string $password
     * @return false|string|null
     */
    private function _prepareUserPassword(string $password): string
    {
        return password_hash($password, config('auth.password_hash_algo'));
    }
}