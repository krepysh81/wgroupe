<?php


namespace Wgroupe\App\Repositories\Post;


use Wgroupe\App\Models\Post;

class PostRepository
{
    private $post;

    /**
     * PostRepository constructor.
     */
    public function __construct()
    {
        $this->post = new Post();
    }

    /**
     * @return array|bool|object|null
     */
    public function paginate()
    {
        return $this->post->getAll();
    }

    /**
     * @param string $postName
     * @return array
     */
    public function getPost(string $postName)
    {
        return $this->post->getByTitle($postName);
    }

    public function updatePost()
    {

    }

}