<?php


namespace Wgroupe;


use Wgroupe\Modules\Router\RouterModule;

class WorkerMachine extends DI
{
    public function __construct()
    {
        $this->registerBaseModules();
    }

    private function registerBaseModules(): void
    {
        // Register router base module
        $this->register('router', function () {
            return new RouterModule($this);
        });
    }

    /**
     * Start machine
     *
     * @throws \Exception
     */
    public function start(): void
    {
        foreach ($this->getModules() as $moduleName => $module)
        {
            if (!method_exists($module, 'register'))
            {
                throw new \Exception("Method register not exist in {$moduleName} extension");
            }

            $module->register();
        }
    }

}