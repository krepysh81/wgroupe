<?php

if (!function_exists('dd'))
{
    /**
     * Speeds up the display of content on the screen
     * Simple alternative Laravel dd
     * @param $variable
     */
    function dd($variable): void
    {
        var_dump($variable); die();
    }
}

if (!function_exists('configs'))
{
    /**
     * Get all configs from folder src/configs
     * @param $variable
     */
    function getConfigs(): array
    {
        $configs = [];

        requireAll(getConfigDir(), function ($fileName, $fileContent) use (&$configs) {
            $configs[array_key_first($fileContent)] = current($fileContent);
        });

        return $configs;
    }
}

if (!function_exists('config'))
{
    /**
     * Get config from file by name
     *
     * @param string $needConfigValue
     * @return mixed
     */
    function config(string $needConfigValue)
    {
       $needConfigValue = explode('.', $needConfigValue);

        function getArrayPath(array $path, array $deepArray) {
            $reduce = function(array $xs, $x) {
                return (
                array_key_exists($x, $xs)
                ) ? $xs[$x] : null;
            };
            return array_reduce($path, $reduce, $deepArray);
        }

        return getArrayPath($needConfigValue, getConfigs());
    }
}