<?php

use Wgroupe\Modules\Router\Components\Response;

if (!function_exists('http_error_404'))
{
    function http_error_404(): void
    {
        header('HTTP/1.0 404 Not Found');
        exit();
    }
}

if (!function_exists('http_error_405'))
{
    function http_error_405(): void
    {
        header('HTTP/1.0 405 Method Not Allowed');
        exit();
    }
}

if (!function_exists('response'))
{
    function response(): Response
    {
        return new Response();
    }
}