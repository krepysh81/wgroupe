<?php

if (!function_exists('getAppDir'))
{
    /**
     * @return string
     */
    function getAppDir(): string
    {
        return dirname(dirname(__DIR__ ) . '../') . DIRECTORY_SEPARATOR;
    }
}

if (!function_exists('getConfigDir'))
{
    /**
     * @return string
     */
    function getConfigDir(): string
    {
        return getAppDir() . 'configs' . DIRECTORY_SEPARATOR;
    }
}

if (!function_exists('getRouterDir'))
{
    /**
     * @return string
     */
    function getRouterDir(): string
    {
        return getAppDir() . 'routers' . DIRECTORY_SEPARATOR;
    }
}