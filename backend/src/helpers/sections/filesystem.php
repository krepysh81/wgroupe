<?php

if (!function_exists('requireFile'))
{
    /**
     * @param string $path
     * @return mixed
     * @throws Exception
     */
    function requireFile(string $path)
    {
        if (!file_exists($path)) {
            throw new Exception("File $path not found");
        }

        return require_once $path;
    }
}

if (!function_exists('requireAll'))
{
    /**
     * Require all files from path
     *
     * @param string $path
     * @param callable $callback
     * @throws Exception
     */
    function requireAll(string $path, ?callable $callback)
    {
        $files = array_diff(scandir($path), ['.', '..']);

        foreach ($files as $file) {;
            $callback($file, requireFile($path . $file));
        }
    }
}