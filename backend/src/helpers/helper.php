<?php

$sectionPath = 'sections' . DIRECTORY_SEPARATOR;

require_once $sectionPath . 'path.php';
require_once $sectionPath . 'filesystem.php';
require_once $sectionPath . 'http.php';
require_once $sectionPath . 'app.php';