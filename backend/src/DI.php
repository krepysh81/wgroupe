<?php


namespace Wgroupe;

/**
 * Container Dependency injection
 *
 * @package App\Models\Container
 */
class DI
{
    /**
     * All modules initialize in app here
     *
     * @var array
     */
    private $modules = [];

    /**
     * Register new module
     *
     * @param string $moduleName
     * @param callable $callback
     */
    protected function register(string $moduleName, callable $callback): void
    {
        $this->modules[$moduleName] = $callback();
    }

    /**
     * Get module with
     *
     * @param string $moduleName
     * @return mixed|null
     */
    public function __get(string $moduleName)
    {
        return $this->modules[$moduleName] ?? null;
    }

    /**
     * @param string $moduleName
     * @return mixed
     * @throws \Exception
     */
    protected function getModule(string $moduleName)
    {
        if(!array_key_exists($moduleName, $this->modules)) {
            throw new \Exception('Not found');
        }

        return $this->modules[$moduleName];
    }

    /**
     * @return array
     */
    protected function getModules(): array
    {
        return $this->modules;
    }
}