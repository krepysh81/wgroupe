<?php

return [

    'database' => [

        'default_connection' => 'mysql',

        'mysql' => [
            'host'        => 'localhost',
            'port'        => '3306',
            'db_name'     => 'wgroupe',
            'db_user'     => 'root',
            'db_password' => '',
        ]
    ]

];