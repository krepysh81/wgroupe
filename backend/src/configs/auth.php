<?php

return [

    'auth' => [

        'session' => [
            'name' => 'wgroupe_session',
            'expired' => time() + 3600, // 1 hour
        ],

        /**
         * About password hash:
         * https://www.php.net/manual/en/function.password-hash.php
         * Use one from this:
         * PASSWORD_DEFAULT, PASSWORD_BCRYPT, PASSWORD_ARGON2I, PASSWORD_ARGON2ID
         */
        'password_hash_algo' => PASSWORD_DEFAULT
    ]
];
