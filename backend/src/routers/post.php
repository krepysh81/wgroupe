<?php

use Wgroupe\Modules\Router\Components\Route;

return (new Route())
    ->get('post', 'PostController@index')
    ->post('post', 'PostController@store')
    ->get('post/:name', 'PostController@show')
    ->put('post/:name', 'PostController@update');