<?php

use Wgroupe\Modules\Router\Components\Route;

return (new Route())
    ->post('auth/login', 'AuthController@login')
    ->post('auth/registration', 'AuthController@registration');