<?php


namespace Wgroupe\Modules\Router\Components;


class Response {

    public const STATUS_SUCCESS = 'success';
    public const STATUS_ERROR   = 'error';

    /**
     * @param null $data
     * @param int $code
     * @return string
     */
    public function json($data = null, int $code = 200): string
    {
        header_remove();
        http_response_code($code);
        header("Cache-Control: no-transform,public,max-age=300,s-maxage=900");
        header('Content-Type: application/json');

        $status = [
            200 => '200 OK',
            400 => '400 Bad Request',
            422 => 'Unprocessable Entity',
            500 => '500 Internal Server Error'
        ];

        header('Status: ' . $status[$code]);

        $responseBody = [
            'status' => $code < 300 ? self::STATUS_SUCCESS : self::STATUS_ERROR,
        ];

        if ($data) {
            $responseBody['data'] = $data;
        }

        return json_encode($responseBody);
    }

    /**
     * @param string|array $message
     * @return string
     */
    public function validationError($message): string
    {
        return $this->json([
            'error_info' => $message
        ], 400);
    }
}