<?php


namespace Wgroupe\Modules\Router\Components;

/**
 * Class Request
 *
 * @package Wgroupe\Modules\Router\Components
 */
class Request {

    public const GET    = 'GET';
    public const POST   = 'POST';
    public const DELETE = 'DELETE';
    public const PUT    = 'PUT';

    private $storage = [];

    private $server;

    /**
     * Request constructor.
     */
    public function __construct()
    {
        $this->storage = $this->cleanInput(
            $this->_prepareRequest()
        );

        $this->server = $_SERVER;
    }

    /**
     * @param $name
     * @return mixed|string|null
     */
    public function __get($name)
    {
        return $this->storage[$name] ?? null;
    }

    /**
     * @param string $method
     * @return bool
     */
    public function isMethod(string $method): bool
    {
        return $this->getMethod() === $method;
    }

    /**
     * @return string
     */
    public function getMethod(): string
    {
        return $this->server['REQUEST_METHOD'];
    }

    /**
     * Get request url
     *
     * @return string
     */
    public function getURL(): string
    {
        return $this->server['REQUEST_URI'];
    }

    /**
     * @return string
     */
    public function getClearURL(): string
    {
        if (($pos = strpos($this->getURL(), '?')) !== false) {
            return substr($this->getURL(), 0, $pos);
        }

        return $this->getURL();
    }

    /**
     * Get request user agent
     *
     * @return string
     */
    public function getUserAgent(): string
    {
        return $this->server['HTTP_USER_AGENT'];
    }

    /**
     * Get all data
     *
     * @return array
     */
    public function all(): array
    {
        return $this->storage;
    }

    /**
     * Filtered and return data by params
     *
     * @param array $data
     * @return array
     */
    public function only(array $data): ?array
    {
        return array_filter($this->storage, function ($key) use ($data) {
            return in_array($key, $data);
        }, ARRAY_FILTER_USE_KEY);
    }

    /**
     * @return array|null
     */
    private function _prepareRequest(): ?array
    {
        return json_decode(file_get_contents("php://input"), true);
    }

    /**
     * @param $data
     * @return array|string
     */
    private function cleanInput($data)
    {
        if (is_array($data))
        {
            $cleaned = [];
            foreach ($data as $key => $value)
            {
                $cleaned[$key] = $this->cleanInput($value);
            }

            return $cleaned;
        }

        return trim(htmlspecialchars($data, ENT_QUOTES));
    }
}