<?php


namespace Wgroupe\Modules\Router\Components;


use Wgroupe\WorkerMachine;
use SplObjectStorage;

class RouteStorage extends SplObjectStorage {

    private $collection;

    public function __construct(WorkerMachine $machine)
    {
        $dir = getRouterDir();

        if (!is_dir($dir)) {
            return [];
        }

        if (!empty($this->collection)) {
            return $this->collection;
        }

        requireAll($dir, function ($fileName, $fileContent) {
            foreach ($fileContent->getCollection() as $route) {
                self::attach($route);
            }
        });

    }

    public function __destruct()
    {
        $temp = [];
        foreach ($this as $route) {
            $temp[] = $route;
        }

        return $temp;
    }

}