<?php


namespace Wgroupe\Modules\Router;


use Wgroupe\Modules\Module;
use Wgroupe\Modules\Router\Components\Router;
use Wgroupe\Modules\Router\Components\RouteStorage;

/**
 * Class RouterModule
 * Register Router module in Worker machine
 *
 * @package App\Modules\Router
 */
class RouterModule extends Module
{
    /**
     * @return Router
     */
    public function register(): Router
    {
        return new Router(
            new RouteStorage($this->machine)
        );
    }
}