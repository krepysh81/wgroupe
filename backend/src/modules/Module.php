<?php


namespace Wgroupe\Modules;


use Wgroupe\WorkerMachine;

abstract class Module
{
    protected $machine;

    public function __construct(WorkerMachine $machine)
    {
        $this->machine = $machine;
    }

    abstract public function register();
}