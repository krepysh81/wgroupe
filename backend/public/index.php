<?php

use Wgroupe\WorkerMachine;

// Load composer autoloader
require_once dirname(__DIR__) . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php';

// Run application
(new WorkerMachine())->start();