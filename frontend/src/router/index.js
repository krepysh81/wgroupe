import Vue from 'vue'
import VueRouter from 'vue-router'
import Configs from '../config'
import Login from '../views/auth/Login'
import Registration from '../views/auth/Registration'
import PostList from '../views/post/List'
import PostView from '../views/post/View'
import PostCreate from '../views/post/Create'
import PageNotFound from '../views/PageNotFound'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    redirect: { name: 'Blog' }
  },
  {
    path: '/auth/login',
    name: 'Login',
    component: Login,
    meta: {
      title: 'Sign in'
    }
  },
  {
    path: '/auth/registration',
    name: 'Registration',
    component: Registration,
    meta: {
      title: 'Registration'
    }
  },
  {
    path: '/blog',
    name: 'Blog',
    component: PostList,
    meta: {
      title: 'Blog'
    }
  },
  {
    path: '/post/:name',
    name: 'PostView',
    component: PostView,
    meta: {
      title: 'View post'
    }
  },
  {
    path: '/post/create',
    name: 'PostCreate',
    component: PostCreate,
    meta: {
      title: 'Create new post',
      requiresAuth: true
    }
  },
  {
    path: '*',
    name: '404',
    component: PageNotFound,
    meta: {
      title: 'Page not found'
    }
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

// Prepare page title
router.afterEach((to, from) => {
  Vue.nextTick(() => {
    document.title = `${Configs.appName} | ${to.meta.title}`
  })
})

// Check route to access
router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    // this route requires auth, check if logged in
    // if not, redirect to login page.
    // if (!auth.loggedIn()) {
    //   next({
    //     name: 'Login'
    //   })
    // } else {
    next()
    // }
  } else {
    next() // make sure to always call next()!
  }
})

export default router
