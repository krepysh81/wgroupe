export default {
  state: {
    auth: []
  },
  mutations: {
    SET_AUTH (state, data) {
      state.auth.push(data)
    }
  },
  getters: {
    getAuth (state) {
      return state.auth
    },
    checkAuth (state) {
      return !state.auth.length
    }
  },
  actions: {
    setAuth ({ commit }, data) {
      commit('SET_AUTH', data)
    }
  }
}
