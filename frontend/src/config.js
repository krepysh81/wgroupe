
export default {
  appName: process.env.VUE_APP_TITLE,
  apiBaseUrl: process.env.VUE_APP_API_BASE_URL,
  apiResponseStatuses: {
    success: 'success',
    error: 'error'
  }
}
