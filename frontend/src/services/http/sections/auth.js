export default axios => ({

  login (data) {
    return axios.post('/auth/login', data)
  },

  registration (data) {
    return axios.post('/auth/registration', data)
  }

})
