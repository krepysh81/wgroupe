export default axios => ({

  all (filters) {
    return axios.get('/post', { filters })
  },

  show (postName) {
    return axios.get(`/post/${postName}`)
  },

  create (data) {
    return axios.post('/post', data)
  },

  update (postId, data) {
    return axios.put(`/post${postId}`, data)
  }

})
