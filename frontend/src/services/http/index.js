import axios from 'axios'
import { app } from '../../main'
import Config from '../../config'

import Auth from './sections/auth'
import Post from './sections/post'
import Message from './sections/message'

// Set default url
axios.defaults.baseURL = Config.apiBaseUrl

// Listen response and validate status
axios.interceptors.response.use(function (response) {
  return response.data
}, function (error) {
  // Show alert error
  app.$bvToast.toast(error.message, {
    title: 'Server error',
    variant: 'danger'
  })

  return Promise.reject(error)
})

export default {
  auth: Auth(axios),
  post: Post(axios),
  message: Message(axios)
}
